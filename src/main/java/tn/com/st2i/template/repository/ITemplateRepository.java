package tn.com.st2i.template.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import tn.com.st2i.template.model.Template;

@Repository
public interface ITemplateRepository extends JpaRepository<Template, Long> {

}
