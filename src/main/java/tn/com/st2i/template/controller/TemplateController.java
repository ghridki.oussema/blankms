package tn.com.st2i.template.controller;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import tn.com.st2i.template.model.Template;
import tn.com.st2i.template.tools.model.SearchObject;
import tn.com.st2i.template.tools.model.SendObject;
import tn.com.st2i.template.service.ICommonService;
import tn.com.st2i.template.service.ISendWsService;
import tn.com.st2i.template.tools.ConstanteWs;

@RestController
@RequestMapping(value = "/app")
@CrossOrigin(origins = "*")
public class TemplateController {

	private static final Logger logger = LoggerFactory.getLogger(TemplateController.class);

	@Autowired
	private ISendWsService sendWsService;

	@Autowired
	private ICommonService commonService;

	@Operation(summary = "Get list of template application")
	@ApiResponses(value = { @ApiResponse(responseCode = ConstanteWs._CODE_WS_SUCCESS, description = "Success"),
			@ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR_ALIAS_PARAM, description = "Un ou plus paramètre(s) est null", content = @Content),
			@ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR_NOT_EXISTS_ROW_DATA_BASE, description = "Template n'existe plus dans l'application", content = @Content),
			@ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR, description = "Erreur du service", content = @Content) })
	@PostMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getListTemplate(HttpServletRequest request, @RequestBody SearchObject obj) {
		try {
			return sendWsService.sendResult(request, commonService.getListPaginator(obj, new Template(), null));
		} catch (Exception argEx) {
			logger.error("Error AdministrationController in method getListTemplate :: " + argEx.toString());
			return sendWsService.sendResultException(request);
		}
	}
	
	@GetMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> eventTemplate(HttpServletRequest request) {
//		HttpServletRequest request = null;
		try {
			JSONObject jsonResult = new JSONObject();
			jsonResult.put("code", "200");
			jsonResult.put("payload", "hello world");
			SendObject so = new SendObject("200", jsonResult, HttpStatus.OK, null);
			return sendWsService.sendResult(request, so);
		} catch (Exception argEx) {
			logger.error("Error AdministrationController in method eventTemplate :: " + argEx.toString());
			return sendWsService.sendResultException(null);
		}
	}
	
	@GetMapping(value = "/test1", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> eventTemplate1(HttpServletRequest request) {
		try {
			Long idUser = this.commonService.getIdAdmUserFromGateway(request);
			JSONObject jsonResult = new JSONObject();
			jsonResult.put("code", "200");
			jsonResult.put("payload", "hello world 123");
			jsonResult.put("id", idUser);
			SendObject so = new SendObject("200", jsonResult, HttpStatus.OK, null);
			return sendWsService.sendResult(request, so);
		} catch (Exception argEx) {
			logger.error("Error AdministrationController in method eventTemplate1 :: " + argEx.toString());
			return sendWsService.sendResultException(null);
		}
	}

}
