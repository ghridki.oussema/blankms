package tn.com.st2i.template.service.impl;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import tn.com.st2i.template.tools.model.LogEvent;
import tn.com.st2i.template.tools.model.SendObject;
import tn.com.st2i.template.service.ISendWsService;
import tn.com.st2i.template.tools.ConstanteWs;

@Service
public class SendWsService implements ISendWsService {
	
	@Value("${spring.application.name}")
	private String nameService;
	 
	 @Autowired
	private RestTemplate restTemplate;

	private static final Logger logger = LogManager.getLogger(SendWsService.class);

	@Override
	public ResponseEntity<?> sendResult(HttpServletRequest request, SendObject so) {
		try {
			LogEvent logEvent = new LogEvent(request.getRemoteAddr()+":"+request.getRemotePort(), 
					request.getHeader("Authorization"), request.getRequestURI(), request.getMethod(), nameService, so.getCode());
			try {
				Executor executor = Executors.newSingleThreadExecutor();
				executor.execute(new Runnable() {
					public void run() {
						ResponseEntity<SendObject> os = restTemplate.postForEntity(
								"http://gateway-service/intern/logData/traceability", logEvent, SendObject.class);
					}
				});				
			} catch (Exception e) {
				logger.error("Error SendWsService in method sendResult/send to gateway  :: " + e.toString());
			}
			return new ResponseEntity<>(so.getPayload().toString(), new HttpHeaders(), so.getHttp());
		} catch (Exception argEx) {
			logger.error("Error SendWsService in method sendResult :: " + argEx.toString());
			return new ResponseEntity<>(so.getPayload().toString(), new HttpHeaders(), so.getHttp());
		}
	}

	@Override
	@SuppressWarnings("finally")
	public ResponseEntity<?> sendResultException(HttpServletRequest request) {
		try {
			LogEvent logEvent = new LogEvent(request.getRemoteAddr()+":"+request.getRemotePort(), 
					request.getHeader("Authorization"), request.getRequestURI(), request.getMethod(), nameService, ConstanteWs._CODE_WS_ERROR);
		} catch (Exception argEx) {
			logger.error("Error SendWsService in method sendResult :: " + argEx.toString());
		} finally {
			return new ResponseEntity<>(new JSONObject(), new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Override
	public ResponseEntity<?> sendResultPublic(HttpServletRequest request, SendObject so) {
		return new ResponseEntity<>(so.getPayload().toString(), new HttpHeaders(), so.getHttp());
	}

}
