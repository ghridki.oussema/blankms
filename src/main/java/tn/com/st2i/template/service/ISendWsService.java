package tn.com.st2i.template.service;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.ResponseEntity;

import tn.com.st2i.template.tools.model.SendObject;


public interface ISendWsService {

	public ResponseEntity<?> sendResult(HttpServletRequest request, SendObject so);
	
	public ResponseEntity<?> sendResultException(HttpServletRequest request);
	
	public ResponseEntity<?> sendResultPublic(HttpServletRequest request, SendObject so);

}