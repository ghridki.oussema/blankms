package tn.com.st2i.template.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "t_template", schema = "template")
public class Template {

	@SequenceGenerator(allocationSize = 1, initialValue = 1, sequenceName = "seq_template", name = "seq_template")
	@GeneratedValue(generator = "seq_template", strategy = GenerationType.SEQUENCE) 
	@Id
	@Column(name = "ID_TEMPLATE", unique = true, nullable = false)
	private Long idTemplate;
	
	@Column(name = "CODE")
	private String code;
	
	@Column(name = "DESG")
	private String desg;

}
